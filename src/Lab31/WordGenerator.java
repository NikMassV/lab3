package Lab31;

import java.util.Random;

class WordGenerator {
    private Random r = new Random(); // Intialize a Random Number Generator with SysTime as the seed

    String getWord() {
        int wordLength = r.nextInt(8) + 3;
        char[] letters = new char[wordLength];
        for (int i = 0; i < wordLength; i++) { // For each letter in the word
            letters[i] = (char) ('a' + r.nextInt(26)); // Generate a letter between a and z
        }
        return new String(letters);//transformation to String
    }
}


