package Lab31;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Input number words");
        int n = in.nextInt();

        WordGenerator word = new WordGenerator();
        String[] words = new String[n];

        for (int i = 0; i < n; i++)
            words[i] = word.getWord();
        WordPrinter.print(words, "Initial order");

        Arrays.sort(words);
        WordPrinter.print(words, "Alphabet order");

        Arrays.sort(words, new WordSorterByLength());
        WordPrinter.print(words, "Sorted by length order");

        WordSorterByLengthLessAverage wordsByLengthLessAverage = new WordSorterByLengthLessAverage();
        WordPrinter.print(wordsByLengthLessAverage.getStringWordsByLengthLessAverage(words), "Printed by length less average order");
    }
}

