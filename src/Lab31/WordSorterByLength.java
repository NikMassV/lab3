package Lab31;

import java.util.Comparator;

class WordSorterByLength implements Comparator<String> {

    public int compare(String ob1, String ob2) {
        return ob1.length() - ob2.length();
    }
}
