package Lab31;

class WordPrinter {
    private static final String RED_COLOR_OUTPUT = "\033[31m";
    private static final String WHITE_COLOR_OUTPUT = "\033[37m";

    static void print(String[] words, String header) {
        System.out.println(RED_COLOR_OUTPUT + header + ": ");
        for (String word : words) {
            System.out.print(WHITE_COLOR_OUTPUT + word + " ");
        }
        System.out.println();
    }
}
