package Lab31;

//чувствую ты увидешь решение в два раза проще. Плохо что длина массива должна определяться при его создании и не может быть расширена.
class WordSorterByLengthLessAverage {
    private double average;
    private double numberSymbolsInString = 0;
    private double numberWords = 0;
    private int newNumberWords = 0;
    private int wordCounter = 0;
    private String[] stringWordsByLengthLessAverage;


    String[] getStringWordsByLengthLessAverage(String[] words) {
        for (String word : words) {
            numberSymbolsInString += word.length();
            numberWords++;
        }
        average = numberSymbolsInString / numberWords;

        for (int i = 0; i < words.length; i++) {
            if (words[i].length() < average) {
                newNumberWords++;
            }
        }

        stringWordsByLengthLessAverage = new String[newNumberWords];
        for (int i = 0; i < words.length; i++) {
            if (words[i].length() < average) {
                stringWordsByLengthLessAverage[wordCounter] = words[i];
                wordCounter++;
            }
        }
        return stringWordsByLengthLessAverage;
    }
}
//это я пытался надурить систему)))
/*stringWordsByLengthLessAverage = new String[newNumberWords];
        newNumberWords++;
        stringWordsByLengthLessAverage[wordCounter] = words[i];
        wordCounter++;*/